// Programa de impressão de texto.
#include <iostream> // permite que o programa gere saída de dados na tela

// a função main inicia a execução do programa

int max(int x, int y){
  if(y>x){
    return y;
  }
  return x;
}
int min(int x, int y){
  if(y<x){
    return y;
  }
  return x;
}

bool intersecta(int rcoordenadas[4], int scoordenadas[4]){
  return !(rcoordenadas[0]>=scoordenadas[2] ||
            rcoordenadas[1]>=scoordenadas[3] ||
            rcoordenadas[2]<=scoordenadas[0] ||
            rcoordenadas[3]<=scoordenadas[1]);
}
void pontosintersecao(int rcoordenadas[4], int scoordenadas[4], int* ponto){
  ponto[0] = max(rcoordenadas[0],scoordenadas[0]); //maior esquerda
  ponto[1] = max(rcoordenadas[1],scoordenadas[1]); //maior inferior
  ponto[2] = min(rcoordenadas[2],scoordenadas[2]); //menor direita
  ponto[3] = min(rcoordenadas[3],scoordenadas[3]); //menor superior
  return;
}

int main(){
  int r[4], rh, rw; // recebe o x e o y do ponto inferior esquerdo, altura e largura respectivamente
  int s[4], sh, sw; // recebe o x e o y do ponto inferior esquerdo, altura e largura respectivamente
  int pontos[4];
  std::cout << "Digite as coordenadas(x,y) do Retângulo R depois sua altura e largura:\n"; // exibe a mensagem
  std::cin >> r[0] >> r[1] >> rh >> rw;
  std::cout << "Digite as coordenadas(x,y) do Retângulo s depois sua altura e largura:\n"; // exibe a mensagem
  std::cin >> s[0] >> s[1] >> sh >> sw;
  r[2] = r[0]+rw;
  r[3] = r[1]+rh;
  s[2] = s[0]+sw;
  s[3] = s[1]+sh;
  if (intersecta(r,s)){
    std::cout << "Intersecta\n";
    pontosintersecao(r,s, pontos);
    std::cout << "XE: " << pontos[0] << " YE: " << pontos[1] <<" XD: " << pontos[2] << " YD: " << pontos[3]<< std::endl;
  }else{
    std::cout << "Não Intersecta\n";
  }

  return 0; // indica que o programa terminou com sucesso
} // fim da função main
